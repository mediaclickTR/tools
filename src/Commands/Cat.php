<?php

namespace Mediapress\Tools\Commands;

use Illuminate\Filesystem\Filesystem;
use Recca0120\Terminal\Console\Commands\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class Cat
 * @package Mediapress\Tools\Commands
 */
class Cat extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dump file';

    /**
     * $files.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * __construct.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Handle the command.
     *
     * @return void
     * @throws \InvalidArgumentException
     */
    public function handle(): void
    {
        $path = $this->argument('path');
        $text = $this->option('text');
        $root = function_exists('base_path') === true ? base_path() : getcwd();
        $path = rtrim($root, '/') . '/' . $path;

        if (is_null($text) === false) {
            $this->files->put($path, $text);
        } else {
            $this->line($this->files->get($path));
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments(): array
    {
        return [
            ['path', InputArgument::REQUIRED, 'path'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return [
            ['text', null, InputOption::VALUE_OPTIONAL],
        ];
    }
}
