<?php

namespace Mediapress\Tools;

use Arcanedev\LogViewer\Http\Routes\LogViewerRoute;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Mediapress\Modules\Module as ServiceProvider;
use Mediapress\Tools\Commands\Cat;


class ToolsServiceProvider extends ServiceProvider
{

    public const RESOURCES = 'Resources';
    public const VIEWS = 'views';
    public $namespace = 'Mediapress\Tools';

    public function boot()
    {
        $this->map();
        $this->loadViewsFrom(__DIR__.DIRECTORY_SEPARATOR.self::RESOURCES.DIRECTORY_SEPARATOR.'views/panel', 'ToolsView');
        $this->publishes([__DIR__.'/Config' => config_path()], "ToolsPublishes");
        $this->loadTranslationsFrom(__DIR__. DIRECTORY_SEPARATOR . 'Resources/lang', 'ToolsPanel');

        $this->mergeConfig(__DIR__ . '/Config/tools_module_actions.php', 'tools_module_actions');
    }

    public function register()
    {
        Config::set('log-viewer.route.attributes.prefix', 'mp-admin/Tools/logs');
        Config::set('log-viewer.route.attributes.middleware', ['web', 'panel.auth']);

        Config::set('terminal.route', [
            'prefix' => 'mp-admin/Tools/terminal',
            'as' => 'Tools.terminal.',
            'middleware' => ['web', 'panel.auth'],
        ]);

        Config::set('terminal.commands', [
            \Recca0120\Terminal\Console\Commands\Artisan::class,
            // \Recca0120\Terminal\Console\Commands\ArtisanTinker::class,
            // \Recca0120\Terminal\Console\Commands\Cleanup::class,
            \Recca0120\Terminal\Console\Commands\Composer::class,
            \Recca0120\Terminal\Console\Commands\Find::class,
            \Recca0120\Terminal\Console\Commands\Mysql::class,
            \Recca0120\Terminal\Console\Commands\Tail::class,
            \Recca0120\Terminal\Console\Commands\Vi::class,
            Cat::class
        ]);
         Config::set('terminal.enabled',true);
    }

    public function map()
    {
        $this->mapPanelRoutes();
    }

    protected function mapPanelRoutes()
    {
        $routes_file =  __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'panel.php';
        Route::middleware('web')
            ->namespace( $this->namespace . '\Controllers\Panel')
            ->prefix( 'mp-admin')
            ->group($routes_file);

    }
}
