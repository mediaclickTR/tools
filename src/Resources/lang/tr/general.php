<?php

return [

    "cache" => "Cache Temizle",
    "cache_click" => "Cache Temizlendi !!",
    "config" => "Config Temizle",
    "config_click" => "Config Temizlendi !!",
    "view" => "View Temizle",
    "view_click" => "View Temizlendi !!",
    "update" => "MP Update",
    "update_click" => "Komut Çalıştırıldı !!",
    "terminal" => "Uç Birim",
    "env_editor" => ".env Düzenleme",
    "backup_tools" => "Yedekleme Araçları",
    "laravel_logs" => "Laravel Logları",
    "vcs" => "Versiyon Kontrol Sistemi",


    "interaction" => [
        'title' => "Interaction Ayarları",
    ],

    "rocket_chat" => [
        'title' => "RocketChat Ayarları",
    ],

    "gtmetrix" => [
        "name" => "GtMetrix Optimizasyonu",
        "placeholder" => "GtMetrix Rapor Url Adresini Giriniz...",
        "button" => "Gönder",
        "error" => "Hata meydana geldi !! </b>",
        "success" => "Gerekli dosyalar başarıyla optimize edildi. </b>",
    ],
    "uploads" => [
        "create_uploads_zip" => "Uploads'ı ziple",
        "create_sql_backup" => "SQL Yedeği Al",
        "creator" => "Oluşturan",
        "type" => "Tip",
        "name" => "Ad",
        "date" => "Tarih",
        "size" => "Boyut",
        "action" => "İşlemler",
        "system" => "Sistem",
        "download" => "İndir",
        "restore" => "Geri Yükle",
        "delete" => "Sil",
    ],
    "save" => "Kaydet",
];
