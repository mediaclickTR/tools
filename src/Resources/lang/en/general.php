<?php

return [

    "cache" => "Clear Cache",
    "cache_click" => "Cache Cleaned !!",
    "config" => "Clear Config",
    "config_click" => "Config Cleared !!",
    "view" => "Clear View",
    "view_click" => "View Cleaned !!",
    "update" => "MP Update",
    "update_click" => "Command Executed !!",
    "terminal" => "Terminal",
    "env_editor" => ".env Editor",
    "backup_tools" => "Backup Tools",
    "laravel_logs" => "Laravel Logs",
    "vcs" => "Version Control System",
    "gtmetrix" => [
        "name" => "GtMetrix Optimization",
        "placeholder" => "Enter the GtMetrix Report Url Address...",
        "button" => "Send",
        "error" => "An error occurred !! </b>",
        "success" => "The required files have been successfully optimized. </b>",
    ],
    "uploads" => [
        "create_uploads_zip" => "Zip Uploads",
        "create_sql_backup" => "SQL Backup",
        "creator" => "Creator",
        "type" => "Type",
        "name" => "Name",
        "date" => "Date",
        "size" => "Size",
        "action" => "Actions",
        "system" => "System",
        "download" => "Download",
        "restore" => "Restore",
        "delete" => "Delete",
    ],
    "save" => "Save",

    "rocket_chat" => [
        'title' => "RocketChat Settings",
    ],
];
