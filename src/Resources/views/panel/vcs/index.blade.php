@extends('MPCorePanel::inc.app')
@section('content')

    @include('MPCorePanel::inc.breadcrumb')
    <div class="row col-12">
        <div class="page-content">
            <div class="row">
                <table class="table col-12">
                    <tr>
                        <th colspan="2">Project Details</th>
                    </tr>
                    <tr>
                        <td>Repository</td>
                        <td style="float: right">
                            @if(strpos($remote,'bitbucket')!==false)
                                <img src="https://bitbucket.org/favicon.ico?v=2" alt="" height="22">
                            @endif
                            <a href="{!! $remote !!}" target="_blank">{!! $remote !!}</a></td>
                    </tr>
                    <tr>
                        <td>Deploy Branch</td>
                        <td>
                            <form action="{!! route('Tools.vcs.checkout') !!}" method="post">
                                {!! csrf_field() !!}

                                <button class="btn btn-primary" style="margin-left: 10px;float: right" type="submit">
                                    Checkout
                                </button>


                                <select name="branch">
                                    @foreach($branches as $branch)
                                        <option value="{!! $branch !!}" {!! $branch == $currentBranch ? 'selected' :'' !!}>{!! $branch !!}</option>
                                    @endforeach
                                </select>


                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>Last Commit Id</td>
                        <td>
                            <div style="float: right">
                                <a target="_blank"
                                   href="{!! $remote.'/commits/'.$lastCommit['commit'] !!}">{!! mb_substr($lastCommit['commit'],0,8) !!}</a>
                                <span title="{!! $lastCommit['date'] !!}">{!! $lastCommit['date']->diffForHumans() !!}</span>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>


            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link " id="status-tab" data-toggle="tab" href="#status" role="tab"
                       aria-controls="status" aria-selected="true">Status ({{ sizeof($status,1)-4 }}) </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="commits-tab" data-toggle="tab" href="#commits" role="tab"
                       aria-controls="commits" aria-selected="false">Commits ({{ count($commits) }})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="logs-tab" data-toggle="tab" href="#logs" role="tab"
                       aria-controls="logs" aria-selected="false">Logs</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade " id="status" role="tabpanel" aria-labelledby="status-tab">

                    <form action="{!! route('Tools.vcs.commit') !!}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-8" style="float:left">

                            <input type="text" name="message" required="required" class="form-control">
                        </div>
                        <div class="col-4" style="float:left">
                            <button class="btn btn-primary" type="submit">
                                Commit and Push
                            </button>
                            <a href="{!! route('Tools.vcs.track') !!}" class="btn btn-primary">Track Files</a></td>
                        </div>


                    </form>
                    <table class="table">
                        @foreach($status['deleted'] as $file)
                            <tr class="file-deleted">
                                <td>Deleted</td>
                                <td>{!! $file !!}</td>
                            </tr>
                        @endforeach

                        @foreach($status['modified'] as $file)
                            <tr class="file-modified">
                                <td>Modified</td>
                                <td>{!! $file !!}</td>
                            </tr>
                        @endforeach

                        @foreach($status['not_tracked'] as $file)
                            <tr class="file-not-tracked">
                                <td>Not Tracked</td>
                                <td>{!! $file !!}</td>
                            </tr>
                        @endforeach
                        @foreach($status['new_file'] as $file)
                            <tr class="file-new-file">
                                <td>New File</td>
                                <td>{!! $file !!}</td>
                            </tr>
                        @endforeach
                    </table>


                </div>
                <div class="tab-pane fade show active" id="commits" role="tabpanel" aria-labelledby="commits-tab">

                    <form action="{!! route('Tools.vcs.index') !!}" method="get">
                        <select name="origin" id="origin">

                            @foreach($origins as $origin)
                                <option value="{!! $origin !!}" {!! $getOrigin == $origin ? 'selected' : (!$getOrigin && 'origin/'.$currentBranch == $origin ? 'selected' : '')  !!}>{!! $origin !!}</option>
                            @endforeach

                        </select>
                        vs {!! $currentBranch !!} diff
                    </form>
                    <table class="table">
                        <tr>
                            <th>Author</th>
                            <th>Commit</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        @foreach($commits as $commit)
                            <tr>
                                <td><img class="avatar_commit" src="{!! get_gravatar($commit['author']['email']) !!}"
                                         alt=""> {!! $commit['author']['name'] !!}</td>
                                <td><a target="_blank"
                                       href="{!! $remote.'/commits/'.$commit['commit'] !!}">{!! mb_substr($commit['commit'],0,8) !!}</a>
                                </td>
                                <td>{!! \Str::limit($commit['message'],30) !!}</td>
                                <td>
                                    <span title="{!! $commit['date'] !!}">{!! $commit['date']->diffForHumans() !!}</span>
                                </td>
                                <td>
                                    <a href="{!! route('Tools.vcs.merge',['commitId'=>$commit['commit'],'branch'=>str_replace('origin/','',$getOrigin)]) !!}" class="btn btn-primary">Pull
                                        and Merge</a>
                                    <a href="{!! $remote.'/branches/compare/'.$commit['commit'].'..'.$lastCommit['commit'].'#diff' !!}"
                                       class="btn btn-primary" target="_blank"> Compare</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>


                </div>
                <div class="tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs-tab">
                    <table class="table">
                        <tr>
                            <th>Author</th>
                            <th>Commit</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        @foreach($logs as $log)
                            @if($loop->first)
                                @continue;
                                @endif
                            <tr>
                                <td><img class="avatar_commit" src="{!! get_gravatar($log['author']['email']) !!}"
                                         alt=""> {!! $log['author']['name'] !!}</td>
                                <td><a target="_blank"
                                       href="{!! $remote.'/commits/'.$log['commit'] !!}">{!! mb_substr($log['commit'],0,8) !!}</a>
                                </td>
                                <td>{!! \Str::limit($log['message'],30) !!}</td>
                                <td>
                                    <span title="{!! $log['date'] !!}">{!! $log['date']->diffForHumans() !!}</span>
                                </td>
                                <td>
                                    <a href="{!! route('Tools.vcs.revert',$log['commit']) !!}"
                                       class="btn btn-primary">Revert</a>
                                    <a href="{!! $remote.'/branches/compare/'.$lastCommit['commit'].'..'.$log['commit'].'#diff' !!}"
                                       class="btn btn-primary" target="_blank"> Compare</a>
                                </td>

                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('styles')
    <style>
        .file-deleted td {
            background-color: #f5f5f5;
            color: #75797f;
        }

        .file-modified td {
            background-color: #fffcdc;
            color: #75797f;

        }

        .file-not-tracked td {
            background-color: #ffd3d3;
            color: #75797f;

        }

        .file-new-file td {
            background-color: #d2ffd2;
            color: #75797f;

        }

        table tbody tr td:nth-child(1) {
            width: inherit;
        }

        .avatar_commit {
            border-radius: 50%;
            width: 32px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('#origin').on('change',function () {
            $(this).parents('form').submit();
        })
    </script>
@endpush
