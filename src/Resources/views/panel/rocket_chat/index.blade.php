@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="row col-12">
        <div class="page-content">
            <form action="{!! route('Tools.rocket_chat.update') !!}" method="post">
                {!! csrf_field() !!}

                <h2>{!! __('ToolsPanel::general.rocket_chat.title') !!}</h2>

                <div class="mt-4">
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Durumu</label>
                        <div class="col-md-6">
                            <select id="status" class="form-control" name="status">
                                <option value="1" {{ config('veronlogin.rocket.status') ? 'selected' : '' }}>Aktif</option>
                                <option value="2" {{ config('veronlogin.rocket.status') ? '' : 'selected' }}>Pasif</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="developerMail" class="col-sm-2 col-form-label">Mail Adresi</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="developerMail" name="developer_mail" value="{{ config('veronlogin.rocket.developer_mail') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="localChannel" class="col-sm-2 col-form-label">Local Error Kanalı</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="localChannel" name="exception_channel_local" value="{{ config('veronlogin.rocket.exception_channel_local') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="testChannel" class="col-sm-2 col-form-label">Test Error Kanalı</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="testChannel" name="exception_channel_test" value="{{ config('veronlogin.rocket.exception_channel_test') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="prodChannel" class="col-sm-2 col-form-label">Canlı Error Kanalı</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="prodChannel" name="exception_channel_prod" value="{{ config('veronlogin.rocket.exception_channel_prod') }}">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col">
                            <button type="submit"
                                    class="btn btn-primary float-right">{!! __('ToolsPanel::general.save') !!}
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
