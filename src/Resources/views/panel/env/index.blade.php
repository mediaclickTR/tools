@extends('MPCorePanel::inc.app')
@section('content')

    @include('MPCorePanel::inc.breadcrumb')
    <div class="row col-12">
        <div class="page-content">
            <form action="{!! route('Tools.env.update') !!}" method="post">
                {!! csrf_field() !!}

                <h2>{!! __('ToolsPanel::general.env_editor') !!}</h2>
                <div class="form-row mt-3">
                    <div class="col">
                        <textarea class="form-control" name="env" id="robot" rows="50">{!! $env !!}</textarea>
                    </div>
                </div>

                <div class="form-row mt-2">
                    <div class="col">
                        <button type="submit"
                                class="btn btn-primary float-right">{!! __('ToolsPanel::general.save') !!}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
