@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="row col-12">
        <div class="page-content">
            <form action="{!! route('Tools.interaction.update') !!}" method="post">
                {!! csrf_field() !!}

                <h2>{!! __('ToolsPanel::general.interaction.title') !!}</h2>

                <div class="row mt-4 mb-3">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="driver">SQL Bağlantı</label>
                            <select class="form-control" name="driver" id="driver">
                                <option value="">Seçim Yapınız</option>
                                <option {{ config('mediapress.interaction.driver') == 'sqlite' ? 'selected' : '' }} value="sqlite">SQLite</option>
                                <option {{ config('mediapress.interaction.driver') == 'mysql' ? 'selected' : '' }} value="mysql">MySql</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {{ config('mediapress.interaction.driver') == 'mysql' ? '' : 'd-none' }}" id="databaseBox">
                            <label for="database">Veri Tabanı Adı</label>
                            <input type="text"
                                   class="form-control"
                                   name="database"
                                   id="database"
                                   placeholder="mediapress_local"
                                   value="{{ config('mediapress.interaction.driver') == 'mysql' ? config('mediapress.interaction.database') : '' }}">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-4">
                    <div class="col">
                        <button type="submit"
                                class="btn btn-primary float-right">{!! __('ToolsPanel::general.save') !!}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('[name="driver"]').on('change', function () {
            if($(this).val() == 'mysql') {
                $('#databaseBox').removeClass('d-none');
            } else {
                $('#databaseBox').addClass('d-none');
                $('[name="database"]').val('');
            }
        })
    </script>
@endpush
