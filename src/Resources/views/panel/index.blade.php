@extends('MPCorePanel::inc.app')
@push('styles')
    <style>
        .form-group input {
            transition: 0.2s;
            outline: 0;
            height: 50px;
            width: 50%;
            background: none;
            border: none;
            border-bottom: 1px solid #b5b5b5;
            padding: 12px 0 0;
            font: 13px Comfortaa-Regular;
        }
        p.message {
            display: block;
            color: rgb(100, 100, 100);
            font: 15px Comfortaa-Regular;
            padding: 20px 0px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="bd-example white mb-5">
                <div class="p-4 text-center">
                    <a href="#" onclick="cacheClear()" class="btn btn-outline-primary cache"> {!! __("ToolsPanel::general.cache") !!}</a>
                    <a href="#" onclick="configClear()" class="btn btn-outline-primary config"> {!! __("ToolsPanel::general.config") !!}</a>
                    <a href="#" onclick="viewClear()" class="btn btn-outline-primary view"> {!! __("ToolsPanel::general.view") !!}</a>
                    <a href="#" onclick="mpUpdate()" class="btn btn-outline-primary mp-update"> {!! __('ToolsPanel::general.update') !!}</a>

                    <hr>

                    <a href="{!! route('Tools.env.index') !!}" class="btn btn-outline-primary"> {!! __('ToolsPanel::general.env_editor') !!}</a>
                    <a href="{!! route('Tools.interaction.index') !!}" class="btn btn-outline-primary"> {!! __('ToolsPanel::general.interaction.title') !!}</a>
                    <a href="{!! route('Tools.rocket_chat.index') !!}" class="btn btn-outline-primary"> {!! __('ToolsPanel::general.rocket_chat.title') !!}</a>
                    @if(Route::has('composer.index'))
                        <a href="{!! route('composer.index') !!}" class="btn btn-outline-primary"> Composer Update</a>
                    @endif
                    <hr>
                    <a href="{!! route('Tools.terminal.index') !!}" class="btn btn-outline-primary" target="_blank"> {!! __('ToolsPanel::general.terminal') !!}</a>
                    <a href="{!! route('Tools.uploads.index') !!}" class="btn btn-outline-primary"> {!! __('ToolsPanel::general.backup_tools') !!}</a>
                    <a href="{!! route('log-viewer::dashboard') !!}" class="btn btn-outline-primary" target="_blank"> {!! __('ToolsPanel::general.laravel_logs') !!}</a>
                    <a href="{!! route('Tools.vcs.index') !!}" class="btn btn-outline-primary"> {!! __('ToolsPanel::general.vcs') !!}</a>
                    <a href="#" onclick="showUrl()" class="btn btn-outline-primary gtmetrix"> {!! __('ToolsPanel::general.gtmetrix.name') !!}</a>
                    <p class="message" style="display: none"></p>
                </div>
                <div class="form-group" style="text-align: center; display: none">
                    <input type="text" id="url" placeholder="{!! __('ToolsPanel::general.gtmetrix.placeholder') !!}">
                    <button class="btn btn-secondary" onclick="gtmetrix()">{!! __('ToolsPanel::general.gtmetrix.button') !!}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ajaxStart(function() {
            $('<div id="_pblloading"><style> @-webkit-keyframes rotate{100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes rotate{100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@-webkit-keyframes chasingBallBounce{50%{-webkit-transform:scale(0);transform:scale(0)}}@keyframes chasingBallBounce{50%{-webkit-transform:scale(0);transform:scale(0)}}.site_loading{height:100%;width:100%;position:fixed;z-index:9999999999999999999999999999;background-color:rgba(0,0,0,.3);left:0;top:0;font-size:150px;color:#fff;display:none}.arc,.arc_dis{position:absolute}.arc_dis,.arc_dis:before{z-index:999999999999999999999999999999}.arc::after,.arc::before,.arc_dis:before{position:absolute;background-color:#bfbfbf}.arc_dis{left:calc(50% - 25px);top:calc(50% - 25px);width:58px;height:58px;-moz-border-radius:100px;-webkit-border-radius:100px;border-radius:100px;-moz-box-shadow:0 0 5px #696969;-webkit-box-shadow:0 0 5px #696969;box-shadow:0 0 5px #696969;background-color:#f5f5f5}.arc_dis:before{content:"";width:4px;height:4px;left:calc(50% - 2px);top:calc(50% - 2px);border-radius:100px;-moz-border-radius:100px;-webkit-border-radius:100px}.arc{margin:0;border:2px solid #ddd;width:50px;height:50px;-moz-border-radius:100px;-webkit-border-radius:100px;border-radius:100px;padding:0;left:calc(50% - 25px);top:calc(50% - 25px)}.arc::after,.arc::before{content:\'\';top:4%;left:48%;width:4%;height:46%;-webkit-transform-origin:50% 100%;-ms-transform-origin:50% 100%;transform-origin:50% 100%;-moz-border-radius:100px;-webkit-border-radius:100px;border-radius:100px;-webkit-animation:rotate 2s infinite linear;animation:rotate 2s infinite linear}.arc::after{height:36%;top:14%;-webkit-animation-duration:12s;animation-duration:12s} </style> <div class="site_loading"> <div class="arc_dis"> <div class="arc"></div> </div> </div> <div id="site_locked"></div>  </div>').prependTo('body');
            $(".site_loading").show();
        }).ajaxStop(function() {
            $("#_pblloading").remove();
            $(".site_loading").hide();
        });

        function cacheClear() {
            $.get("{!! route('Tools.cache.index') !!}", function (data) {
                $('.cache').removeClass('btn-outline-primary');
                $('.cache').css('background', "#21ad5c");
                $('.cache').css('color', "#fff");
                $('.cache').text("{!! __('ToolsPanel::general.cache_click') !!}!");

                setTimeout(function() {
                    $('.cache').addClass('btn-outline-primary');
                    $('.cache').text("{!! __('ToolsPanel::general.cache') !!}");
                }, 2000)
            });
        }

        function configClear() {
            $.get("{!! route('Tools.config.index') !!}", function (data) {
                $('.config').removeClass('btn-outline-primary');
                $('.config').css('background', "#21ad5c");
                $('.config').css('color', "#fff");
                $('.config').text("{!! __('ToolsPanel::general.config_click') !!}!");

                setTimeout(function() {
                    $('.config').addClass('btn-outline-primary');
                    $('.config').text("{!! __('ToolsPanel::general.config') !!}");
                }, 2000)
            });
        }

        function viewClear() {
            $.get("{!! route('Tools.view.index') !!}", function (data) {
                $('.view').removeClass('btn-outline-primary');
                $('.view').css('background', "#21ad5c");
                $('.view').css('color', "#fff");
                $('.view').text("{!! __('ToolsPanel::general.view_click') !!}!");

                setTimeout(function() {
                    $('.view').addClass('btn-outline-primary');
                    $('.view').text("{!! __('ToolsPanel::general.view') !!}");
                }, 2000)
            });
        }

        function mpUpdate() {
            $.get("{!! route('Tools.mpupdate.index') !!}", function (data) {
                $('.mp-update').removeClass('btn-outline-primary');
                $('.mp-update').css('background', "#21ad5c");
                $('.mp-update').css('color', "#fff");
                $('.mp-update').text("{!! __('ToolsPanel::general.update_click') !!}");


                setTimeout(function() {
                    $('.mp-update').addClass('btn-outline-primary');
                    $('.mp-update').text("{!! __('ToolsPanel::general.update') !!}");
                }, 5000)
            });
        }

        function showUrl() {
            $('.form-group').toggle();
        }

        function gtmetrix() {
            $.get("{!! route('Tools.gtmetrix.index') !!}", {url: $('#url').val()}, function (data) {
                $('.form-group').hide();
                $('p.message').show();
                $('p.message').html(data);
            })
        }
    </script>
@endpush
