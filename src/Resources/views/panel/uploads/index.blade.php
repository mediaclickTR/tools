@extends('MPCorePanel::inc.app')
@section('content')

    @include('MPCorePanel::inc.breadcrumb')
    <div class="row col-12">
        <div class="page-content">

            <a href="{!! route('Tools.uploads.create') !!}" class="btn btn-primary">{!! __("ToolsPanel::general.uploads.create_uploads_zip") !!}</a>
            <a href="{!! route('Tools.uploads.createSQL') !!}" class="btn btn-primary"> {!! __("ToolsPanel::general.uploads.create_sql_backup") !!}</a>

            <table class="table">
                <tr>
                    <td>{!! __('ToolsPanel::general.uploads.creator') !!}</td>
                    <td>{!! __('ToolsPanel::general.uploads.type') !!}</td>
                    <td>{!! __('ToolsPanel::general.uploads.name') !!}</td>
                    <td>{!! __('ToolsPanel::general.uploads.date') !!}</td>
                    <td>{!! __('ToolsPanel::general.uploads.size') !!}</td>
                    <td>{!! __('ToolsPanel::general.uploads.action') !!}</td>
                </tr>
                @foreach($list as $item)
                    <tr>
                        @if($item['user'])
                            <td>
                                <img class="gravatar"
                                     src="{!! get_gravatar($item['user']->email) !!}"
                                     alt="">{!! $item['user']->shortName !!}
                            </td>
                        @else
                            <td>{!! __('ToolsPanel::general.uploads.system') !!}</td>
                        @endif
                        <td>
                            @if(strpos($item['name'],'sql') !==false)
                                <i class="fa fa-database"></i> &nbsp;SQL
                            @else
                                <i class="fa fa-file-archive"></i> &nbsp;UPLOADS
                            @endif
                        </td>


                        <td>{!! $item['name'] !!}</td>
                        <td>
                            <span title="{!! $item['date'] !!}">{!! $item['date']->diffForHumans() !!}</span>
                        </td>
                        <td>{!! $item['size'] !!}</td>
                        <td>
                            <a href="{!! route('Tools.uploads.download',$item['name']) !!}"
                               class="btn btn-primary">{!! __('ToolsPanel::general.uploads.download') !!}</a>

                            @if(strpos($item['name'],'sql') !== false)
                                <a href="{!! route('Tools.uploads.restore',$item['name']) !!}"
                                   class="btn btn-warning restore">{!! __('ToolsPanel::general.uploads.restore') !!}</a>
                            @endif

                            <a href="{!! route('Tools.uploads.delete',$item['name']) !!}" class="btn btn-danger">{!! __('ToolsPanel::general.uploads.delete') !!}</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>


@endsection

@push('styles')
    <style>
        table tbody tr td:nth-child(1) {
            width: inherit;
        }

        .gravatar {
            margin-right: 10px;
            border-radius: 50%;
            width: 22px;
        }
    </style>

@endpush

@push('scripts')
    <script>
        $('a.restore').on('click', function (e) {

            var person = prompt("Veritabanını restore etmek için, `restore` yazınız !!");
            if (person !== "restore") {
                e.preventDefault();
                alert("Veritabanı restore edilmedi.")
            }

        })
    </script>
@endpush
