<?php

namespace Mediapress\Tools;


use Mediapress\Models\MPModule;

class Tools extends MPModule
{
    public $name = "Tools";
    public $url = "mp-admin/Tools";
    public $description = "Tools";
    public $author = "";
    public $menus = [];
    public $plugins = [];
}
