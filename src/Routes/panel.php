<?php

Route::group(['prefix' => 'Tools', 'middleware' => 'panel.auth', 'as' => 'Tools.'], function () {
    Route::get('/', 'ToolsController@index')->name('index');

    Route::group(['prefix' => 'vcs', 'as' => 'vcs.'], function () {
        Route::get('/', 'VCSController@index')->name('index');
        Route::get('/merge/{commitId}/{branch?}', 'VCSController@merge')->name('merge');
        Route::get('/track', 'VCSController@track')->name('track');
        Route::get('/revert/{commitId}', 'VCSController@merge')->name('revert');
        Route::post('/commit', 'VCSController@commit')->name('commit');
        Route::post('/checkout', 'VCSController@checkout')->name('checkout');

    });

    Route::group(['prefix' => 'uploads', 'as' => 'uploads.'], function () {
        Route::get('/', 'UploadController@index')->name('index');
        Route::get('create', 'UploadController@create')->name('create');
        Route::get('createSQL', 'UploadController@createSQL')->name('createSQL');
        Route::get('download/{file}', 'UploadController@download')->name('download');
        Route::get('restore/{file}', 'UploadController@restore')->name('restore');
        Route::get('delete/{file}', 'UploadController@delete')->name('delete');
    });

    Route::group(['prefix' => 'env'], function () {
        Route::get('/', 'EnvController@index')->name('env.index');
        Route::post('update', 'EnvController@update')->name('env.update');
    });

    Route::get('/cache', 'CacheController@index')->name('cache.index');
    Route::get('/config', 'ConfigController@index')->name('config.index');
    Route::get('/view', 'ViewController@index')->name('view.index');
    Route::get('/mp-update', 'MPUpdateController@index')->name('mpupdate.index');
    Route::get('/gtmetrix', 'GtMetrixController@index')->name('gtmetrix.index');


    Route::group(['prefix' => 'interaction', 'as' => 'interaction.'], function () {
        Route::get('/', 'InteractionController@index')->name('index');
        Route::post('update', 'InteractionController@update')->name('update');
    });

    Route::group(['prefix' => 'RocketChat', 'as' => 'rocket_chat.'], function () {
        Route::get('/', 'RocketChatController@index')->name('index');
        Route::post('update', 'RocketChatController@update')->name('update');
    });
});

Route::get('guard/{md5}/{function?}', [
    'as' => 'guard',
    'uses' => 'GuardController@guard'
]);
