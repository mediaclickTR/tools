<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Artisan;
use Mediapress\Modules\Content\Facades\Content;

class RocketChatController
{
    public function index(Request $request)
    {

        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.rocket_chat",
                "text" => __('ToolsPanel::general.rocket_chat.title'),
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);

        return view('ToolsView::rocket_chat.index', compact('breadcrumb'));
    }

    public function update(Request $request) {

        $status = $request->get('status') == 1 ? 'true' : 'false';
        $mail = $request->get('developer_mail');
        $localChannel = $request->get('exception_channel_local');
        $testChannel = $request->get('exception_channel_test');
        $prodChannel = $request->get('exception_channel_prod');

        $env = file_get_contents(base_path('.env'));


        if (strpos($env, "ROCKET_STATUS") === false) {
            $temp = $env . "\nROCKET_STATUS=" . $status . "\n";
        } else {
            $temp = preg_replace('/\nROCKET_STATUS=(.*?)\n/', "\nROCKET_STATUS=" . $status . "\n", $env);
        }

        if (strpos($temp, "ROCKET_DEVELOPER_MAIL") === false) {
            $temp .= "ROCKET_DEVELOPER_MAIL=" . $mail . "\n";
        } else {
            $temp = preg_replace('/\nROCKET_DEVELOPER_MAIL=(.*?)\n/', "\nROCKET_DEVELOPER_MAIL=" . $mail . "\n", $temp);
        }

        if (strpos($temp, "ROCKET_LOCAL_CHANNEL") === false) {
            $temp .= "ROCKET_LOCAL_CHANNEL=" . $localChannel . "\n";
        } else {
            $temp = preg_replace('/\nROCKET_LOCAL_CHANNEL=(.*?)\n/', "\nROCKET_LOCAL_CHANNEL=" . $localChannel . "\n", $temp);
        }

        if (strpos($temp, "ROCKET_TEST_CHANNEL") === false) {
            $temp .= "ROCKET_TEST_CHANNEL=" . $testChannel . "\n";
        } else {
            $temp = preg_replace('/\nROCKET_TEST_CHANNEL=(.*?)\n/', "\nROCKET_TEST_CHANNEL=" . $testChannel . "\n", $temp);
        }

        if (strpos($temp, "ROCKET_PROD_CHANNEL") === false) {
            $temp .= "ROCKET_PROD_CHANNEL=" . $prodChannel . "\n";
        } else {
            $temp = preg_replace('/\nROCKET_PROD_CHANNEL=(.*?)\n/', "\nROCKET_PROD_CHANNEL=" . $prodChannel . "\n", $temp);
        }


        file_put_contents(base_path('.env'), $temp);

        return redirect(route('Tools.index'));
    }
}
