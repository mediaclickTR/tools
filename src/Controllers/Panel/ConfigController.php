<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Artisan;

/**
 * Class CacheController
 * @package Mediapress\Tools\Controllers\Panel
 */
class ConfigController
{
    /**
     * @param Request $request
     * @return void
     */
    public function index(Request $request): void
    {
        try {
            Artisan::call('config:clear');
        } catch (\Exception $exception) {
            dd($exception);
        }
    }
}
