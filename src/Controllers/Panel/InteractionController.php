<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Artisan;
use Mediapress\Modules\Content\Facades\Content;

class InteractionController
{
    public function index(Request $request)
    {

        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.interaction",
                "text" => __('ToolsPanel::general.interaction.title'),
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);

        return view('ToolsView::interaction.index', compact('breadcrumb'));
    }

    public function update(Request $request) {

        $driver = $request->get('driver');

        if($driver == 'sqlite') {
            $database = '"' . storage_path('app/database/Interaction.sqlite') . '"';
        } else {
            $database = $request->get('database');
        }

        $env = file_get_contents(base_path('.env'));


        if (strpos($env, "INTERACTION_DRIVER") === false) {
            $temp = $env . "\nINTERACTION_DRIVER=" . $driver . "\n";
        } else {
            $temp = preg_replace('/\nINTERACTION_DRIVER=(.*?)\n/', "\nINTERACTION_DRIVER=" . $driver . "\n", $env);
        }

        if (strpos($temp, "INTERACTION_DATABASE") === false) {
            $temp .= "INTERACTION_DATABASE=" . $database . "\n";
        } else {
            $temp = preg_replace('/\nINTERACTION_DATABASE=(.*?)\n/', "\nINTERACTION_DATABASE=" . $database . "\n", $temp);
        }

        file_put_contents(base_path('.env'), $temp);


        if($driver == 'mysql') {
            Artisan::call('migrate', ['--path' => "vendor/mediapress/mediapress/src/Modules/Interaction/Database/mysql-migrations", '--force' => true]);
        }

        return redirect(route('Tools.index'));
    }
}
