<?php

namespace Mediapress\Tools\Controllers\Panel;

use Carbon\Carbon;
use Cz\Git\GitRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Mediapress\Modules\Content\Facades\Content;

/**
 * Class VCSController
 * @package Mediapress\Tools\Controllers\Panel
 */
class VCSController
{

    public const MESSAGE = 'message';
    public const VCS_INDEX = 'Tools.vcs.index';
    public const ORIGIN = 'origin';
    public const ORIGIN1 = 'origin/';

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {

        $getOrigin = $request->origin;
        $repo = $this->getRepo();

        $status = $this->getStatus($repo);

        $repo->fetch(self::ORIGIN);

        $remote = $this->getRepoUrl($repo);

        $lastCommit = $repo->getCommitData($repo->getLastCommitId());
        $lastCommit['date'] = Carbon::parse($lastCommit['date'])->setTimezone('Europe/Istanbul');
        $currentBranch = $repo->getCurrentBranchName();
        $origins = $repo->getRemoteBranches();
        $branches = array_merge($repo->getLocalBranches(), $origins);


        $commits = $this->commitLogs($repo, $getOrigin);

        $logs = collect($this->logs($repo))->slice(0, 10);


        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.env",
                "text" => __('ToolsPanel::general.vcs'),
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);


        return view('ToolsView::vcs.index',
            compact('remote', 'branches', 'currentBranch', 'commits', 'status', 'logs', 'lastCommit', 'origins',
                'getOrigin', 'breadcrumb'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function commit(Request $request): RedirectResponse
    {
        $repo = $this->getRepo();

        if ($repo->hasChanges()) {
            $repo->addAllChanges();
            $repo->commit($request->message);
        }

        $branch = $repo->getCurrentBranchName();
        $repo->push('origin ' . $branch);

        return redirect(route(self::VCS_INDEX));
    }

    /**
     * @param string $commitId
     * @param string|null $brach
     * @return RedirectResponse
     */
    public function merge(string $commitId, string $brach = null): RedirectResponse
    {
        $repo = $this->getRepo();
        $repo->fetch(self::ORIGIN);
        if ($brach) {
            $current = $brach;
        } else {
            $current = $repo->getCurrentBranchName();
        }

        $repo->merge(self::ORIGIN1 . $current);

        $repo->execute(['reset', mb_substr($commitId, 0, 8), '--hard']);
        return redirect(route(self::VCS_INDEX) . '?origin=' . urlencode(self::ORIGIN1 . $current));
    }

    /**
     * @param $commitId
     * @return RedirectResponse
     */
    public function revert($commitId): RedirectResponse
    {
        $repo = $this->getRepo();

        $repo->execute(['reset', mb_substr($commitId, 0, 8), '--hard']);
        return redirect(route(self::VCS_INDEX));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function checkout(Request $request): RedirectResponse
    {
        $repo = $this->getRepo();
        $branch = $request->branch;

        if (strpos($branch, self::ORIGIN1) !== false) {
            $repo->checkout($branch);
            $repo->checkout(str_replace(self::ORIGIN1, '', $branch));
        } else {
            $repo->checkout($branch);
        }
        return redirect(route(self::VCS_INDEX));
    }

    /**
     * @return RedirectResponse
     */
    public function track(): RedirectResponse
    {
        $repo = $this->getRepo();

        if ($repo->hasChanges()) {
            $repo->addAllChanges();
        }
        return redirect(route(self::VCS_INDEX));
    }

    /**
     * @param GitRepository $repo
     * @param $getOrigin
     * @return array
     */
    private function commitLogs(GitRepository $repo, $getOrigin): array
    {
        if ($getOrigin) {
            $commits = $repo->execute(['log', 'HEAD..' . $getOrigin]);
        } else {
            $current = $repo->getCurrentBranchName();

            $commits = $repo->execute(['log', 'HEAD..origin/' . $current]);
        }

        return $this->commitsToArray($commits);
    }

    /**
     * @param GitRepository $repo
     * @return array
     */
    private function logs(GitRepository $repo): array
    {
        $commits = $repo->execute('log');
        return $this->commitsToArray($commits);

    }

    /**
     * @param GitRepository $repo
     * @return array|array[]
     */
    private function getStatus(GitRepository $repo): array
    {
        $status = ['deleted' => [], 'modified' => [], 'not_tracked' => [], 'new_file' => []];
        if ($repo->hasChanges()) {

            $list = $repo->execute('status');

            $track = false;
            foreach ($list as $item) {
                if (strpos($item, 'deleted:') !== false) {
                    $status['deleted'][] = trim(explode('deleted:', $item)[1]);
                } elseif (strpos($item, 'modified:') !== false) {
                    $status['modified'][] = trim(explode('modified:', $item)[1]);
                } elseif (strpos($item, 'new file:') !== false) {
                    $status['new_file'][] = trim(explode('new file:', $item)[1]);
                } else {
                    if (strpos($item, 'include in what will be committed') !== false) {
                        $track = true;
                    } elseif ($track && trim($item) && strpos($item, DIRECTORY_SEPARATOR)) {
                        $status['not_tracked'][] = trim($item);
                    }
                }
            }

        }
        return $status;
    }

    /**
     * @param GitRepository $repo
     * @return mixed|string
     */
    private function getRepoUrl(GitRepository $repo)
    {
        $remote = collect($repo->execute(['remote', '-v']));

        $remote = trim(str_replace([self::ORIGIN], ['',], $remote->first()));

        $remote = explode(' ', $remote)[0];
        if (strpos($remote, 'git@') !== false) {
            $remote = 'https://' . str_replace(['git@', ':', '.git'], ['', '/', ''], $remote);
        }
        return $remote;
    }

    /**
     * @param array $commits
     * @return array
     */
    private function commitsToArray(array $commits): array
    {
        $commitLogs = [];
        $i = -1;
        foreach ($commits as $commit) {
            if (strpos($commit, 'commit ') !== false) {
                $commitLogs[++$i]['commit'] = explode(' ', $commit)[1];
                $commitLogs[$i][self::MESSAGE] = '';
            } elseif (strpos($commit, 'Author:') !== false) {
                $regex = '/(Author: )([A-Za-zöğışçöÖÇŞİĞÜ ]+)[<]([a-zA-Z0-9.@]+)[>]/m';
                preg_match_all($regex, $commit, $matches, PREG_SET_ORDER, 0);

                $commitLogs[$i]['author']['name'] = trim(isset($matches[0][2]) ? $matches[0][2] : '');
                $commitLogs[$i]['author']['email'] = trim(isset($matches[0][3]) ? $matches[0][3] : '');
            } elseif (strpos($commit, 'Date:') !== false) {
                $commitLogs[$i]['date'] = Carbon::parse(explode('Date: ', $commit)[1])->setTimezone('Europe/Istanbul');
            } else {
                $commitLogs[$i][self::MESSAGE] .= $commit;
                $commitLogs[$i][self::MESSAGE] = trim($commitLogs[$i][self::MESSAGE]);
            }
        }
        return $commitLogs;
    }

    /**
     * @return GitRepository
     */
    private function getRepo(): GitRepository
    {
        return new GitRepository(base_path());
    }


}
