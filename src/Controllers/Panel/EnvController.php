<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Mediapress\Modules\Content\Facades\Content;

/**
 * Class EnvController
 * @package Mediapress\Tools\Controllers\Panel
 */
class EnvController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $env = file_get_contents(base_path('.env'));

        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.env",
                "text" => __('ToolsPanel::general.env_editor'),
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);

        return view('ToolsView::env.index', compact('env', 'breadcrumb'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        file_put_contents(base_path('.env'), $request->env);
        return redirect(route('Tools.env.index'));
    }
}
