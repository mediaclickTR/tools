<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Artisan;
use Mediapress\Foundation\Htmldom\Htmldom;

/**
 * Class GtMetrixController
 * @package Mediapress\Tools\Controllers\Panel
 */
class GtMetrixController
{

    /**
     * GtMetrixController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->url = $request['url'];
        $url = explode('/', $this->url);
        if(isset($url[4])){
            $this->mainUrl = [4];
        }
    }


    /**
     * @param Request $request
     * @return string
     */
    public function index(Request $request): string
    {
        try {
            $html = new Htmldom(file_get_contents($this->url));
            $details = $html->find('.rules-details');
            $imageFiles = [];
            $jsFiles = [];
            $cssFiles = [];

            foreach ($details as $detail) {
                $text = $detail->find('div p');
                if (isset($text[0])) {
                    $text = $text[0]->text();
                    if (strpos($text, 'Optimize the following images') !== false) {
                        $imageFiles = $detail->find('div ul li');
                    } elseif (strpos($text, 'Minify JavaScript') !== false) {
                        $jsFiles = $detail->find('div ul li');
                    } elseif (strpos($text, 'Minify CSS') !== false) {
                        $cssFiles = $detail->find('div ul li');
                    }
                }

            }

            $errors = [];

            $this->setImageFiles($imageFiles, $errors);
            $this->setCssFiles($cssFiles, $errors);
            $this->setJsFiles($jsFiles, $errors);


            if (count($errors) > 0) {
                $return = __("ToolsPanel::general.gtmetrix.error");
                foreach ($errors as $error) {
                    $return .= $error + "</b>";
                }
            } else {
                $return = __("ToolsPanel::general.gtmetrix.success");
            }

            Artisan::call('cache:clear');
            Artisan::call('view:clear');

            return $return;
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * @param array $imageFiles
     * @param array $errors
     * @return void
     */
    private function setImageFiles(array $imageFiles, array &$errors): void
    {
        if (! empty($imageFiles)) {
            $imageArray = [];
            foreach ($imageFiles as $image) {
                $matched_link = '';
                $links = $image->find('a');
                if (mb_substr(str_replace(['http://', 'https://'], ['', ''], $links[0]->href), 0, strlen($this->mainUrl)) == $this->mainUrl) {
                    preg_match("/(\/uploads|\/images).*$/", $links[0]->href, $matched_link);
                    if (isset($matched_link[0])) {
                        $imageArray[asset($matched_link[0])] = 'https://gtmetrix.com' . $links[1]->href;
                    }
                }

            }

            foreach ($imageArray as $key => $value) {
                $rename = str_replace(url('/'), "", $key);
                $image = file_get_contents($value);
                if (file_exists(public_path($rename)) && file_put_contents(public_path($rename) . '', $image) === false) {
                    $errors[] = $rename;
                }

            }
        }
    }

    /**
     * @param array $cssFiles
     * @param array $errors
     * @return void
     */
    private function setCssFiles(array $cssFiles, array &$errors): void
    {
        if (! empty($cssFiles)) {
            $cssArray = [];
            foreach ($cssFiles as $image) {
                $links = $image->find('a');
                if (mb_substr(str_replace(['http://', 'https://'], ['', ''], $links[0]->href), 0, strlen($this->mainUrl)) == $this->mainUrl) {
                    $cssArray[$links[0]->href] = 'https://gtmetrix.com' . $links[1]->href;
                }
            }

            foreach ($cssArray as $key => $value) {
                $rename = str_replace(url('/'), "", $key);
                $css = file_get_contents($value);
                if (file_exists(public_path($rename)) && file_put_contents(public_path($rename) . '', $css) === false) {

                    $errors[] = $rename;
                }
            }
        }
    }

    /**
     * @param array $jsFiles
     * @param array $errors
     * @return void
     */
    private function setJsFiles(array $jsFiles, array &$errors): void
    {
        if (! empty($jsFiles)) {
            $jsArray = [];
            foreach ($jsFiles as $image) {
                $links = $image->find('a');
                if (mb_substr(str_replace(['http://', 'https://'], ['', ''], $links[0]->href), 0, strlen($this->mainUrl)) == $this->mainUrl) {
                    $jsArray[$links[0]->href] = 'https://gtmetrix.com' . $links[1]->href;
                }
            }

            foreach ($jsArray as $key => $value) {
                $rename = str_replace(url('/'), "", $key);
                $js = file_get_contents($value);
                if (file_exists(public_path($rename)) && file_put_contents(public_path($rename) . '', $js) === false) {

                    $errors[] = $rename;
                }
            }
        }
    }
}
