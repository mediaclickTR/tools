<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Artisan;

/**
 * Class MPUpdateController
 * @package Mediapress\Tools\Controllers\Panel
 */
class MPUpdateController
{
    /**
     * @param Request $request
     * @return void
     */
    public function index(Request $request): void
    {
        try {
            Artisan::call('mp:update');
        } catch (\Exception $exception) {
            dd($exception);
        }
    }
}
