<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\MPCore\Models\MDisk;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Artisan;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class UploadController
 * @package Mediapress\Tools\Controllers\Panel
 */
class UploadController
{
    public const APP_PUBLIC_STORAGE = 'vendor/storage/';

    public const APP_PUBLIC_STORAGE1 = 'vendor/storage';

    public const UPLOADS_INDEX = 'Tools.uploads.index';

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $this->checkDirectory();

        $directory = public_path(self::APP_PUBLIC_STORAGE1);

        $listCommand = 'cd ' . $directory . ' && ';
        $listCommand .= 'ls -lah';
        $process = new Process($listCommand);
        $process->run();

        $list = $this->makeArrayForFiles($process->getOutput());


        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.env",
                "text" => __('ToolsPanel::general.backup_tools'),
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);


        return view('ToolsView::uploads.index', compact('list', 'breadcrumb'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request): RedirectResponse
    {

        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $user_id = Auth::guard('admin')->user()->id;

        $string = 'uploads-' . date("Y-m-d-H-i-s") . '_' . $user_id . '_' . '.zip';

        $this->checkDirectory();
        $uploadsFile = public_path(self::APP_PUBLIC_STORAGE . $string);

        $zipCommand = 'cd ' . public_path() . ' && ';
        $zipCommand .= 'zip -r ' . $uploadsFile . ' uploads/';
        $process = new Process($zipCommand);
        $process->run();

        return redirect(route(self::UPLOADS_INDEX));
    }

    /**
     * @return RedirectResponse
     */
    public function createSQL(): RedirectResponse
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $user_id = Auth::guard('admin')->user()->id;
        $string = public_path('vendor/storage/sql-' . date("Y-m-d-H-i-s") . '_' . $user_id . '_' . '.sql');

        $connection = 'mysqldump -h ' .
            config('tools.db_host') . ' -P ' .
            config('tools.db_port') . ' -u ' .
            config('tools.db_username') . ' -p' .
            config('tools.db_password') . ' ' .
            config('tools.db_database') . ' > ' . $string;


        $process = new Process($connection);
        $process->run();

        return redirect(route(self::UPLOADS_INDEX));
    }


    /**
     * @param string $file
     * @return RedirectResponse
     */
    public function restore(string $file): RedirectResponse
    {
        set_time_limit(0);

        try {

            $table_names = \DB::select("SELECT TABLE_NAME FROM information_schema.TABLES WHERE `TABLE_SCHEMA` LIKE '" . config('tools.db_database') . "'");

            $names = array();
            foreach ($table_names as $name) {
                $names[] = $name->TABLE_NAME;
            }
            $names = implode(', ', $names);

            for ($i = 1; $i <= 10; $i++) {
                try {
                    \DB::statement("DROP TABLE IF EXISTS " . $names);
                } catch (\Exception $exception) {
                }
            }

            $string = public_path('vendor/storage/' . $file);
            $connection = 'mysql -h ' . config('tools.db_host') . ' -P ' . config('tools.db_port') . ' -u ' . config('tools.db_username') . ' -p' . config('tools.db_password') . ' ' . config('tools.db_database') . ' < ' . $string;

            $process = new Process($connection);
            $process->run();

        } catch (\Exception $exception) {
            dd($exception);
        }

        return redirect(route(self::UPLOADS_INDEX));
    }

    /**
     * @param string $name
     * @return RedirectResponse
     */
    public function delete(string $name): RedirectResponse
    {
        @unlink(public_path(self::APP_PUBLIC_STORAGE . $name));
        return redirect(route(self::UPLOADS_INDEX));
    }

    /**
     * @param string $name
     * @return BinaryFileResponse
     */
    public function download(string $name): BinaryFileResponse
    {
        return response()->download(public_path(self::APP_PUBLIC_STORAGE . $name));
    }

    /**
     * @return void
     */
    private function checkDirectory(): void
    {
        $directory = public_path(self::APP_PUBLIC_STORAGE1);
        if (!file_exists($directory)) {
            mkdir(public_path(self::APP_PUBLIC_STORAGE1), 0777, true);
        }
    }

    /**
     * @param string $getOutput
     * @return array
     */
    private function makeArrayForFiles(string $getOutput): array
    {
        $outputs = explode("\n", $getOutput);

        $list = [];


        foreach ($outputs as $key => $output) {

            if ($key >= 3 && $output) {
                $fileList = collect(explode(' ', $output));


                if (count($fileList)) {

                    $name = $fileList->last();

                    $date = filemtime(public_path(self::APP_PUBLIC_STORAGE . $name));
                    $re = '/[_](.*)[_]/m';
                    $str = $name;

                    preg_match($re, $str, $matches);

                    $admin = null;
                    if (isset($matches[1])) {
                        $admin = Admin::find($matches[1]);

                    }


                    $list[] = [
                        'name' => $name,
                        'size' => $this->human_filesize(filesize(public_path(self::APP_PUBLIC_STORAGE . $name))),
                        'date' => Carbon::createFromTimestamp($date)->setTimezone('Europe/Istanbul'),
                        'user' => $admin

                    ];
                }

            }
        }
        return $list;
    }

    /**
     * @param string $bytes
     * @param int $decimals
     * @return string
     */
    public function human_filesize(string $bytes, int $decimals = 2): string
    {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }


}
