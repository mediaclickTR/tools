<?php

namespace Mediapress\Tools\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ToolsController
 * @package Mediapress\Tools\Controllers\Panel
 */
class ToolsController
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        return view('ToolsView::index');
    }
}
